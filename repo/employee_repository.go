package repo

import (
	"encoding/json"
	"fmt"
	"github.com/go-resty/resty/v2"
	"github.com/jmoiron/sqlx"
	"gitlab.com/farolina.r/cis-auth/domain"
	"gitlab.com/farolina.r/cis-auth/logger"
	"gitlab.com/farolina.r/cis-auth/resp"
	"net/http"
	"os"
)

const MaxGetEmployeeDataCount = 25
const HRAppsURLEmployee = "http://hr-apps.int.cbn.net.id/ClientFile/employee/"

type EmployeeRepositoryDb struct {
	client      *sqlx.DB
	restyClient *resty.Client
}

func NewEmployeeRepositoryDb(db *sqlx.DB, restyClient *resty.Client) *EmployeeRepositoryDb {
	return &EmployeeRepositoryDb{
		client:      db,
		restyClient: restyClient,
	}
}

func (a *EmployeeRepositoryDb) GetProfileClient(accessToken string) (*domain.EmployeeProfileResponse, *resp.ErrorResponse) {
	employeeHost := os.Getenv("EMPLOYEE_APP_HOST")
	employeePort := os.Getenv("EMPLOYEE_APP_PORT")

	a.restyClient.SetContentLength(true)
	response, err := a.restyClient.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", fmt.Sprintf("Bearer %s", accessToken)).
		Get(fmt.Sprintf("%s:%s/api/v1/user/Profile", employeeHost, employeePort))

	if err != nil {
		logger.Error(err.Error())
		return nil, resp.InternalError()
	}

	if response.StatusCode() != http.StatusOK {
		logger.Error(fmt.Sprintf("error getting profile data: %v", response.String()))
		return nil, resp.ResponseStatusError(response.StatusCode(), "")
	}

	var respData domain.EmployeeProfileResponse
	err = json.Unmarshal([]byte(response.String()), &respData)
	if err != nil {
		logger.Error(err.Error())
		return nil, resp.InternalError()
	}

	return &respData, nil
}

func (a *EmployeeRepositoryDb) GetAccessTokenClient(request *domain.EmployeeAuthRequest) (*domain.EmployeeAuthResponse, *resp.ErrorResponse) {
	employeeHost := os.Getenv("EMPLOYEE_APP_HOST")
	employeePort := os.Getenv("EMPLOYEE_APP_PORT")
	employeeAuth := os.Getenv("EMPLOYEE_APP_AUTH")

	// Sets `Content-Length` header automatically
	a.restyClient.SetContentLength(true)

	// resty will set content-type to `application/x-www-form-urlencoded`
	// automatically
	response, err := a.restyClient.R().
		SetFormData(map[string]string{
			"grant_type": "password",
			"username":   request.Username,
			"password":   request.Password,
		}).
		SetHeader("Cache-Control", "no-cache").
		SetHeader("Authorization", fmt.Sprintf("Basic %s", employeeAuth)).
		Post(fmt.Sprintf("%s:%s/oauth/token", employeeHost, employeePort))

	if err != nil {
		logger.Error(err.Error())
		return nil, resp.InternalError()
	}

	if response.StatusCode() != http.StatusOK {
		logger.Error(fmt.Sprintf("error getting access token: %v", response.String()))
		return nil, resp.ResponseStatusError(response.StatusCode(), "")
	}

	var employee domain.EmployeeAuthResponse
	err = json.Unmarshal([]byte(response.String()), &employee)
	if err != nil {
		logger.Error(err.Error())
		return nil, resp.InternalError()
	}

	return &employee, nil
}
