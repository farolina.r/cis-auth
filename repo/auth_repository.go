package repo

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/doug-martin/goqu/v9"
	"github.com/go-resty/resty/v2"
	"github.com/jmoiron/sqlx"
	"gitlab.com/farolina.r/cis-auth/domain"
	"gitlab.com/farolina.r/cis-auth/helper"
	"gitlab.com/farolina.r/cis-auth/logger"
	"gitlab.com/farolina.r/cis-auth/resp"
	"net/http"
	"time"
)

const ErrorMsgDeviceFull = "Device is full, cannot register device again."
const ErrorMsgEmployeeNotFound = "Employee Data Don't Exist"
const ErrorMsgAppKeyNotValid = "App key not Valid"
const SuccessMsgLoggedOut = "You have successfully Logged Out"

type AuthRepositoryDb struct {
	client       *sqlx.DB
	restyClient  *resty.Client
	employeeRepo *EmployeeRepositoryDb
}

type RemoteAuthRepository struct {
}

func (a AuthRepositoryDb) Logout(session domain.AppTokenSession) (*resp.APIResponse, *resp.ErrorResponse) {
	//	 update AppTokenSession
	ds := goqu.Update("AppTokenSession").Set(goqu.Record{
		"ExpiredAt":   helper.GetCurrentTimeUTC(),
		"UpdatedBy":   session.UserCode,
		"UpdatedHost": session.CreatedHost,
		"UpdatedTime": helper.GetCurrentTimeUTC(),
	}).Where(goqu.Ex{
		"StaffCode": session.UserCode,
		"api_token": session.BlackpineToken,
	})

	query := helper.BuildSQL(ds)
	err := helper.ExecQuery(a.client, query)
	if err != nil {
		return nil, resp.InternalError()
	}

	// delete device
	dds := goqu.Update("EmplyDevices").Set(goqu.Record{
		"ReasonDeleted": "LogOut",
		"DeletedBy":     session.UserCode,
		"UpdatedBy":     session.UserCode,
		"UpdatedHost":   session.CreatedHost,
		"UpdatedTime":   helper.GetCurrentTimeUTC(),
	}).Where(goqu.Ex{
		"StaffCode": session.UserCode,
		"Token":     session.BlackpineToken,
	})

	query = helper.BuildSQL(dds)

	err = helper.ExecQuery(a.client, query)
	if err != nil {
		return nil, resp.InternalError()
	}

	return resp.NewAPIResponse(
		http.StatusOK,
		true,
		SuccessMsgLoggedOut,
		nil,
	), nil
}

func (a AuthRepositoryDb) Login(login domain.Login, host string) (*domain.LoginResponse, *resp.AppError) {
	var response *domain.LoginResponse

	serv := a.employeeRepo
	empAuthResp, errResp := serv.GetAccessTokenClient(&domain.EmployeeAuthRequest{
		Username: login.Username,
		Password: login.Password,
	})
	if errResp != nil {
		return nil, &resp.AppError{
			Code:    errResp.Code,
			Message: errResp.Message,
		}
	}

	profileResponse, errResp := serv.GetProfileClient(empAuthResp.AccessToken)
	if errResp != nil {
		return nil, &resp.AppError{
			Code:    errResp.Code,
			Message: errResp.Message,
		}
	}
	if profileResponse != nil {
		var expired time.Time
		expiresIn := time.Unix(empAuthResp.ExpiresIn, 0)
		expired = expiresIn.UTC()
		if expiresIn.Before(time.Now()) {
			// add 24 hours
			expired = time.Now().Add(domain.ExpirationTime).UTC()
		}

		var loginUserData domain.LoginUserData

		if len(profileResponse.Data) > 0 {
			d, err := json.Marshal(profileResponse)
			if err != nil {
				return nil, &resp.AppError{
					Code:    http.StatusInternalServerError,
					Message: resp.InternalError().Message,
				}
			}

			err = json.Unmarshal(d, &loginUserData)
			if err != nil {
				logger.Error(err.Error())
				return nil, &resp.AppError{
					Code:    http.StatusInternalServerError,
					Message: resp.InternalError().Message,
				}
			}
		}

		// set expired at for login user data
		loginUserData.ExpiresIn = helper.AddTimeUTC(time.Now(), domain.ExpirationTime)

		token, err := loginUserData.GenerateToken()
		if err != nil {
			return nil, &resp.AppError{
				Code:    http.StatusInternalServerError,
				Message: resp.InternalError().Message,
			}
		}

		refreshToken, _ := loginUserData.GenerateRefreshToken()

		//var branchCode string
		//if len(profileResponse.Data) > 0 {
		//	branchCode = profileResponse.Data[0].BranchCode
		//}

		var profile domain.EmployeeProfile
		var branchCode string
		if len(profileResponse.Data) > 0 {
			profile = profileResponse.Data[0]
			branchCode = profile.BranchCode
		}

		appErr := a.InsertAppTokenSession(domain.AppTokenSession{
			UserCode:       profile.UserCode,
			BlackpineToken: empAuthResp.AccessToken,
			JWTToken:       token,
			BranchCode:     &branchCode,
			ExpiredAt:      expired,
			CreatedBy:      profile.UserCode,
			CreatedHost:    host,
		})
		if appErr != nil {
			return nil, &resp.AppError{
				Code:    appErr.Code,
				Message: appErr.Message,
			}
		}

		response = &domain.LoginResponse{
			Name:           profile.FullName,
			StatusCode:     http.StatusText(http.StatusOK),
			BlackpineToken: empAuthResp.AccessToken,
			JWTToken:       token,
			RefreshToken:   refreshToken,
			TokenType:      empAuthResp.TokenType,
			ExpiresIn:      expired,
		}
	} else {
		return nil, &resp.AppError{
			Code:    http.StatusBadRequest,
			Message: ErrorMsgEmployeeNotFound,
		}
	}

	return response, nil
}

func (a AuthRepositoryDb) InsertAppTokenSession(appTokenSession domain.AppTokenSession) *resp.ErrorResponse {
	query := fmt.Sprintf(
		"INSERT INTO auth_user_log (UserCode, api_token, jwt_token, BranchCode, ExpiredAt, CreatedBy, CreatedTime, CreatedHost) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
		helper.PutQuotationMark(appTokenSession.UserCode),
		helper.PutQuotationMark(appTokenSession.BlackpineToken),
		helper.PutQuotationMark(appTokenSession.JWTToken),
		helper.PutQuotationMark(*appTokenSession.BranchCode),
		helper.PutQuotationMark(helper.ConvertTimeUTCToString(appTokenSession.ExpiredAt)),
		helper.PutQuotationMark(appTokenSession.CreatedBy),
		helper.PutQuotationMark(helper.GetCurrentTimeUTC()),
		helper.PutQuotationMark(appTokenSession.CreatedHost),
	)

	err := helper.ExecQuery(a.client, query)
	if err != nil {
		return resp.InternalError()
	}

	return nil
}

func (a AuthRepositoryDb) updateJWTAppTokenSession(appTokenSession domain.AppTokenSession, host string) *resp.ErrorResponse {
	ds := goqu.From("AppTokenSession").
		Update().Set(goqu.Record{
		"UpdatedTime": helper.GetCurrentTimeUTC(),
		"UpdatedBy":   appTokenSession.UserCode,
		"UpdatedHost": host,
		"jwt_token":   appTokenSession.JWTToken,
		"ExpiredAt":   helper.ConvertTimeUTCToString(appTokenSession.ExpiredAt),
	}).Where(goqu.Ex{
		"UserCode":  appTokenSession.UserCode,
		"api_token": appTokenSession.BlackpineToken,
		"DeletedBy": nil,
	})
	query := helper.BuildSQL(ds)

	err := helper.ExecQuery(a.client, query)
	if err != nil {
		logger.Error(err.Error())
		return resp.InternalError()
	}

	return nil
}

func (a AuthRepositoryDb) RefreshToken(accessToken string, refreshToken string, host string) (*domain.RefreshAuth, *resp.ErrorResponse) {
	var response *domain.RefreshAuth
	tokenInvalid := false

	// verify jwt refreshToken
	claims, appErr := domain.AuthClaims{Token: refreshToken}.Verify()
	if appErr != nil {
		if appErr.Code == http.StatusUnauthorized {
			tokenInvalid = true
		} else {
			return nil, appErr
		}
	}

	// delete previous session
	if tokenInvalid {
		_, appErr = a.RemoveUserSessionByJWTToken(accessToken, host)
		if appErr != nil {
			return nil, appErr
		}
		return nil, resp.ForbiddenError(resp.ErrRefreshTokenInvalidRetryLogin.Error())
	}

	// get previous session
	session, appErr := a.GetUserSessionByUserCodeAndJWTToken(claims.UserCode, accessToken)
	if appErr != nil {
		if appErr.Code == http.StatusNotFound {
			return nil, resp.OtherError(resp.ErrTokenInvalid)
		}
		return nil, appErr
	}

	loginUserData := domain.LoginUserData{
		UserCode: claims.UserCode,
		Email:    claims.Email,
	}

	newAccessToken, err := loginUserData.GenerateToken()
	if err != nil {
		return nil, resp.InternalError()
	}

	newRefreshToken, _ := loginUserData.GenerateRefreshToken()

	// update session, because api_token is primary key
	appErr = a.updateJWTAppTokenSession(domain.AppTokenSession{
		UserCode:       session.UserCode,
		BlackpineToken: session.BlackpineToken,
		JWTToken:       newAccessToken,
		ExpiredAt:      helper.AddTimeUTC(time.Now(), domain.ExpirationTime),
	}, host)
	if appErr != nil {
		return nil, appErr
	}

	response = &domain.RefreshAuth{
		AccessToken:  newAccessToken,
		RefreshToken: newRefreshToken,
	}

	return response, nil
}

func (a AuthRepositoryDb) GetUserSessionByUserCodeAndJWTToken(userCode string, token string) (*domain.AppTokenSession, *resp.ErrorResponse) {
	var appTokenSession domain.AppTokenSession
	ds := goqu.From("AppTokenSession").
		Order(goqu.I("ExpiredAt").Desc()).
		Where(goqu.Ex{
			"UserCode":  userCode,
			"jwt_token": token,
			//"ExpiredAt": goqu.Op{"gt": helper.GetCurrentTimeUTC()},
			"DeletedBy": nil,
		}).Select(
		"StaffCode", "api_token", "jwt_token", "BranchCode", "ExpiredAt", "CreatedBy", "CreatedHost",
	).Limit(1)
	query := helper.BuildSQL(ds)
	err := a.client.Get(&appTokenSession, query)
	if err != nil {
		logger.Error(fmt.Sprintf("Token: %s, error: %s", token, err.Error()))
		if err == sql.ErrNoRows {
			return nil, resp.NotFoundError()
		}
		return nil, resp.InternalError()
	}

	return &appTokenSession, nil
}

func (a AuthRepositoryDb) RemoveUserSessionByJWTToken(token string, host string) (*resp.APIResponse, *resp.ErrorResponse) {
	var response *resp.APIResponse
	ds := goqu.From("AppTokenSession").
		Update().Set(goqu.Record{
		"UpdatedTime": helper.GetCurrentTimeUTC(),
		"UpdatedHost": host,
		"DeletedBy":   domain.UserSystem,
	}).Where(goqu.Ex{
		"jwt_token": token,
		//"ExpiredAt": goqu.Op{"lt": helper.GetCurrentTimeUTC()},
		"DeletedBy": nil,
	})
	query := helper.BuildSQL(ds)
	err := helper.ExecQuery(a.client, query)
	if err != nil {
		logger.Error(err.Error())
		return nil, resp.InternalError()
	}

	response = resp.NewAPIResponse(http.StatusOK, true, "", nil)

	return response, nil
}

func (a AuthRepositoryDb) GetUserByToken(token string) (*domain.AppTokenSession, *resp.ErrorResponse) {
	var appTokenSession domain.AppTokenSession
	ds := goqu.From("AppTokenSession").Where(goqu.Ex{
		"api_token": token,
		//"ExpiredAt": goqu.Op{"gt": helper.GetCurrentTimeUTC()},
		"DeletedBy": nil,
	}).Select(
		"StaffCode", "api_token", "ExpiredAt", "CreatedBy", "CreatedHost",
	)
	query := helper.BuildSQL(ds)
	err := a.client.Get(&appTokenSession, query)
	if err != nil {
		logger.Error(err.Error())
		if err == sql.ErrNoRows {
			return nil, resp.NotFoundError()
		}
		return nil, resp.InternalError()
	}

	return &appTokenSession, nil
}

func NewAuthRepository(client *sqlx.DB, restyClient *resty.Client, employeeRepo *EmployeeRepositoryDb) domain.AuthRepository {
	return &AuthRepositoryDb{
		client:       client,
		restyClient:  restyClient,
		employeeRepo: employeeRepo,
	}
}
