package resp

import (
	"encoding/json"
	"net/http"
)

const StatusSuccess = "success"
const StatusError = "error"

type APIResponse struct {
	Code    int         `json:"code"`
	Success bool        `json:"success"`
	Status  string      `json:"status"`
	Message string      `json:"message"`
	Content interface{} `json:"content"`
}

type ErrorResponse struct {
	Code    int         `json:"code"`
	Success bool        `json:"success"`
	Status  string      `json:"status"`
	Message string      `json:"message"`
	Content interface{} `json:"content"`
}

func getStatus(status bool) string {
	if status {
		return StatusSuccess
	}
	return StatusError
}

func getSuccess(code int) bool {
	if code == http.StatusOK {
		return true
	}
	return false
}

func WriteResponse(w http.ResponseWriter, resp *APIResponse) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(resp.Code)
	resp.Status = getStatus(resp.Success)
	if err := json.NewEncoder(w).Encode(&resp); err != nil {
		panic(err)
	}
}

func (e *ErrorResponse) ToAPIResponse() *APIResponse {
	return &APIResponse{
		Code:    e.Code,
		Success: getSuccess(e.Code),
		Status:  getStatus(e.Success),
		Message: e.Message,
		Content: e.Content,
	}
}

func NewAPIResponse(code int, success bool, message string, content interface{}) *APIResponse {
	return &APIResponse{
		Code:    code,
		Success: success,
		Status:  getStatus(success),
		Message: message,
		Content: content,
	}
}

func (e ErrorResponse) ToAppError() *AppError {
	return &AppError{
		Message: e.Message,
		Code:    e.Code,
	}
}
