package domain

import (
	"gitlab.com/farolina.r/hris-api-go/resp"
)

type EmployeeRepository interface {
	GetAccessTokenClient(request *EmployeeAuthRequest) (*EmployeeAuthResponse, *resp.ErrorResponse)
	GetProfileClient(accessToken string) (*EmployeeProfileResponse, *resp.ErrorResponse)
}

type EmployeeAuthRequest struct {
	GrantType string `json:"grant_type"`
	Username  string `json:"username"`
	Password  string `json:"password"`
}

type EmployeeAuthResponse struct {
	AccessToken string `json:"access_token"`
	TokenType   string `json:"token_type"`
	ExpiresIn   int64  `json:"expires_in"`
}

type EmployeeProfile struct {
	UserCode    string `json:"UserCode"`
	FullName    string `json:"FullName"`
	DisplayName string `json:"DisplayName"`
	Email       string `json:"Email"`
	EmplId      string `json:"Empl_id"`
	BranchCode  string `json:"BranchCode"`
}

type EmployeeProfileResponse struct {
	Response int               `json:"response"`
	Message  string            `json:"message"`
	Data     []EmployeeProfile `json:"data"`
}

type EmployeeData struct {
	StaffCode int64  `db:"StaffCode"`
	MaxDevice int    `db:"MaxDevice"`
	FullName  string `db:"Fullname"`
}
