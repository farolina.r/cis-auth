package domain

type AppUser struct {
	ID            int     `db:"id"`
	Username      string  `db:"username"`
	RememberToken string  `db:"remember_token"`
	Notes         string  `db:"Notes,omitempty"`
	CreatedBy     string  `db:"CreatedBy,omitempty"`
	CreatedTime   string  `db:"CreatedTime,omitempty"`
	CreatedHost   string  `db:"CreatedHost,omitempty"`
	UpdatedBy     *string `db:"UpdatedBy,omitempty"`
	UpdatedTime   *string `db:"UpdatedTime,omitempty"`
	UpdatedHost   *string `db:"UpdatedHost,omitempty"`
	DeleteBy      *string `db:"DeletedBy,omitempty"`
	ReasonDeleted *string `db:"ReasonDeleted,omitempty"`
}
