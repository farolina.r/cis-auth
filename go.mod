module gitlab.com/farolina.r/cis-auth

go 1.17

require (
	github.com/doug-martin/goqu/v9 v9.18.0
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-resty/resty/v2 v2.7.0
	github.com/golang-jwt/jwt/v4 v4.4.1
	github.com/gorilla/mux v1.8.0
	github.com/jmoiron/sqlx v1.3.5
	github.com/joho/godotenv v1.4.0
	github.com/microsoft/go-mssqldb v0.14.0
	gitlab.com/farolina.r/hris-api-go v0.0.0-20220520035331-946db94412c9
	go.uber.org/zap v1.21.0
)

require (
	github.com/golang-sql/civil v0.0.0-20220223132316-b832511892a9 // indirect
	github.com/golang-sql/sqlexp v0.1.0 // indirect
	github.com/nxadm/tail v1.4.8 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	golang.org/x/crypto v0.0.0-20220525230936-793ad666bf5e // indirect
	golang.org/x/net v0.0.0-20220325170049-de3da57026de // indirect
)
