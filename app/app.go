package app

import (
	"context"
	"fmt"
	"github.com/go-redis/redis"
	resty "github.com/go-resty/resty/v2"
	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	_ "github.com/microsoft/go-mssqldb"
	"gitlab.com/farolina.r/cis-auth/handlers"
	"gitlab.com/farolina.r/cis-auth/helper"
	"gitlab.com/farolina.r/cis-auth/logger"
	"gitlab.com/farolina.r/cis-auth/repo"
	"gitlab.com/farolina.r/cis-auth/service"
	"gitlab.com/farolina.r/cis-auth/store"
	"log"
	"net/http"
	"os"
	"time"
)

func sanityCheck() {
	if os.Getenv("DB_USERNAME") == "" || os.Getenv("DB_PASSWORD") == "" || os.Getenv("DB_HOST") == "" || os.Getenv("DB_PORT") == "" || os.Getenv("DB_DATABASE") == "" {
		log.Fatal("env is not set")
	}
}

func Start() {
	sanityCheck()
	router := mux.NewRouter()
	dbClient := connectToDatabase()
	redisClient := connectToRedis()
	restyClient := connectToResty()

	// handlers
	employeeRepo := repo.NewEmployeeRepositoryDb(dbClient, restyClient)

	authRepo := repo.NewAuthRepository(dbClient, restyClient, employeeRepo)
	authStore := store.NewAuthStore(redisClient, authRepo)
	authHandler := handlers.NewAuthHandler(service.NewAuthService(authStore, authRepo))

	// routes
	router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte("Hello World"))
		if err != nil {
			return
		}
	})

	// un auth routes
	unauthenticatedRoute := router.PathPrefix("/api").Subrouter()

	// auth routes
	//authenticatedRoute := router.PathPrefix("/api").Subrouter()

	// v1 unauth subroute
	v1UnauthSubroute := unauthenticatedRoute.PathPrefix("/v1").Subrouter()

	v1UnauthSubroute.HandleFunc("/login", authHandler.Login).Methods(http.MethodPost)
	//unauthenticatedRoute.HandleFunc("/refreshToken", authHandler.RefreshToken).Methods(http.MethodPost)
	//unauthenticatedRoute.HandleFunc("/locations", masterHandler.GetAttendanceLocations).Methods(http.MethodPost)

	//
	//authenticatedRoute.HandleFunc("/logout", authHandler.Logout).Methods(http.MethodPost)
	//
	//am := middleware.NewAuthMiddleware(authRepo)
	//authenticatedRoute.Use(am.AuthorizationHandler())

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", helper.Getenv("APP_PORT", "8008")), router))
}

func connectToDatabase() *sqlx.DB {
	dbUsername := os.Getenv("DB_USERNAME")
	dbPassword := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_DATABASE")
	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")

	// open db connection
	dataSourceName := fmt.Sprintf("server=%s;user id=%s;password=%s;port=%s;database=%s;", dbHost, dbUsername, dbPassword, dbPort, dbName)
	logger.Info("Connecting to database: " + dataSourceName)
	client, err := sqlx.Open(helper.Getenv("DB_CONNECTION", "sqlserver"), dataSourceName)
	if err != nil {
		log.Fatal("Error creating connection pool: ", err.Error())
	}
	ctx := context.Background()
	err = client.PingContext(ctx)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Connected to database: " + dataSourceName)

	client.SetConnMaxLifetime(time.Minute * 3)
	client.SetMaxOpenConns(10)
	client.SetMaxIdleConns(10)

	return client
}

//
func connectToResty() *resty.Client {
	restyClient := resty.New()
	restyClient.SetTimeout(time.Second * 20)
	restyClient.SetRetryCount(3)
	restyClient.SetRetryWaitTime(time.Second * 3)

	logger.Info("Connecting to resty")

	return restyClient
}

func connectToRedis() *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%s", os.Getenv("REDIS_HOST"), os.Getenv("REDIS_PORT")),
		Password: os.Getenv("REDIS_PASSWORD"),
		DB:       0,
	})
	if err := client.Ping().Err(); err != nil {
		logger.Error("redis connection error: " + err.Error())
		return nil
	}
	logger.Info("Connecting to redis: " + client.Options().Addr)

	return client
}
