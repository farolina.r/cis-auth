package main

import (
	"github.com/joho/godotenv"
	"gitlab.com/farolina.r/cis-auth/app"
	"gitlab.com/farolina.r/cis-auth/logger"
)

func main() {
	// load env with go-dotenv
	err := godotenv.Load()
	if err != nil {
		logger.Fatal("Error loading .env file")
		panic(err)
	}

	app.Start()
}
